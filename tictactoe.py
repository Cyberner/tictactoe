from random import randrange

def display_board(board):
    # The function accepts one parameter containing the board's current status
    # and prints it out to the console.
    f1=board[0][0]
    print("""
        +-------+-------+-------+
        |       |       |       |
        |  """,board[0][0],"""  |  """,board[0][1],"""  |  """,board[0][2],"""  |
        |       |       |       |
        +-------+-------+-------+
        |       |       |       |
        |  """,board[1][0],"""  |  """,board[1][1],"""  |  """,board[1][2],"""  |
        |       |       |       |
        +-------+-------+-------+
        |       |       |       |
        |  """,board[2][0],"""  |  """,board[2][1],"""  |  """,board[2][2],"""  |
        |       |       |       |
        +-------+-------+-------+  
        """)

def make_list_of_free_fields(board): # The function browses the board and builds a list of all the free squares; 
    free_fields=[]
    for row in board:
        for field in row:
            if type(field)==int:
                free_fields.append(field)
    return free_fields
        
def get_index(field_num):
    index_dic = {1:(0,0),2:(0,1),3:(0,2),4:(1,0),5:(1,1),6:(1,2),7:(2,0),8:(2,1),9:(2,2)}
    return index_dic.get(field_num)
   
def draw_move(board):    # The function draws the computer's move and updates the board.
    free_fields = make_list_of_free_fields(board) 
    while True: #loops until rando (random number from 1 to 9 is free and then exits loop with break call)
        rando = randrange(1,10)
        if rando in free_fields:
            row, cell = get_index(rando) #get field index of rando and fill into row and cell variable
            board[row][cell]="X" #set X to field
            break #exit loop
    check_victory(board, rando)
    return(board)

def enter_move(board):
    # The function accepts the board's current status, asks the user about their move, 
    # checks the input, and updates the board according to the user's decision.
    free_fields = make_list_of_free_fields(board) #get list of free fields
    field_num = int(0) #initiate field_num with number not on board
    while field_num not in free_fields: #loop until free valid field is selected
        field_num = int(input("ENTER YOUR MOVE: ")) #aks user to enter fieldnumber
        if field_num in free_fields:
                row, cell = get_index(field_num) #get field index of rando and fill into row and cell variable
                board[row][cell]="O" #set O to field

        else:
            print("That went wrong, I suspect, that you entered a fieldnumber that is not on the board or not free. please choose another field")
    check_victory(board, field_num)
    return(board)

def check_victory(board, newfield): #check if there are any Xes or Oes in a straight line
    global victory
    if newfield in (1,2,3): #check the 1 2 3 row
        if board[0][0] == board[0][1] and board[0][0] == board[0][2]:
            victory = board[0][0]
    if newfield in (4,5,6): #check the 4 5 6 row
        if board[1][0] == board[1][1] and board[1][0] == board[1][2]:
            victory = board[1][0]
    if newfield in (7,8,9): #check the 7 8 9 row
        if board[2][0] == board[2][1] and board[2][0] == board[2][2]:
            victory = board[2][0]
    if newfield in (1,4,7): #check the 1 4 7 column
        if board[0][0] == board[1][0] and board[0][0] == board[2][0]:
            victory = board[0][0]
    if newfield in (2,5,8): #check the 2 5 8 column
        if board[0][1] == board[1][1] and board[0][1] == board[2][1]:
            victory = board[0][1]
    if newfield in (3,6,9): #check the 3 6 9 column
        if board[0][2] == board[1][2] and board[0][2] == board[2][2]:
            victory = board[0][2]
    if newfield in (1,5,9): #check the 1 5 9 cross line
        if board[0][0] == board[1][1] and board[0][0] == board[2][2]:
            victory = board[0][0]
    if newfield in (3,5,7): #check the  3 5 7 cross line
        if board[0][2] == board[1][1] and board[0][2] == board[2][0]:
            victory = board[0][2]

board = [[cell for cell in range(line,line+3)] for line in range(1,10,3)]
victory = "nobody"

display_board(board)
while True:
    board = draw_move(board)
    display_board(board)
    if len(make_list_of_free_fields(board)) <= 0 or victory != "nobody":
        break
    board = enter_move(board)
    display_board(board)
    if len(make_list_of_free_fields(board)) <= 0 or victory != "nobody":
        break
    #print(get_index(2))
    #row, cell = get_index(2)
    #print("row = ", row, "cell = ", cell)

print("!!!!!GAME OVER!!!!!") #print gameover message
if victory == "O": #check if player is the winner
    print("!!!!!YOU WIN!!!!!")
elif victory == "X": #check if computer is the winner
    print("!!!!!YOU LOSE!!!!!")    
else:print("!!!!!DRAW!!!!!") #if nobody is the winner, print draw message

display_board(board)
